# Lab classes for ICL 16/17

This repository contains the started code needed in the Lab classes of the ICL 16/17 course. Students should fork this repository into their private account on bitbucket, and use it as their own repository for developing the course assignments. Code will be added along the semestre 

Os alunos devem fazer um fork no bitbucket para poderem usar como seu repositório particular, e poderem ainda assim actualizá-lo com as alterações propostas. O repositório original vai ser evoluído ao longo do semestre com novo código.

## Lab 1 - Parsing

The code in file `Parser.jj` implements the starting point for the following grammar, following the usual priority for the operators.

~~~
exp ::= exp '+' exp | exp '-' exp | exp '*' exp | exp '+' exp |  '(' exp ')' | num

num ::= ['1'-'9']['0'-'9']*
~~~

Test the given grammar, using the provided `JUNit` classes, and write more tests for it.

### Goal 1

Extend the grammar to have the following extra operations

~~~
exp ::= exp '+' exp | exp '-' exp | exp '*' exp | exp '+' exp | '-' exp | '(' exp ')' | num

cmp ::= exp '<' exp | exp '>' exp | exp '==' exp | exp '!=' exp | ...

bexp ::= bool | cmp | exp '&&' exp | exp '||' exp | '!' exp 

bool ::= true | false

num ::= ['1'-'9']['0'-'9']*
~~~

Extend the semantic actions to compute the values 

### Goal 2

Produce new tests that cover significant cases of the new grammar.

### Goal 3

Implement a simple calculator using the parser semantic actions and an `int` return type.

### Goal 4

Extend the testing infrastructure to accept a value and compare the result to it. 